﻿using System;

namespace MassTransitSagaRequest.Contracts
{
    public record OrderSubmitted(Guid SubmitId);
}