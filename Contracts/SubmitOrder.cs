﻿using System;

namespace MassTransitSagaRequest.Contracts
{
    public record SubmitOrder(Guid Id);
}