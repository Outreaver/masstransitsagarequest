﻿using System;

namespace MassTransitSagaRequest.Contracts
{
    public record StartOrder(Guid Id);
}
