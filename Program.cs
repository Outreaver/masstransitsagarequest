using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MongoDB.Driver;
using System;
using System.Threading.Tasks;

namespace MassTransitSagaRequest
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            await CreateHostBuilder(args).Build().RunAsync();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<BackgroundWorker>();

                    var mongoClient = new MongoClient("mongodb://localhost:27017");
                    var mongoDatabase = mongoClient.GetDatabase("masstransit_sagarequest");

                    services.AddSingleton<IMongoClient>(_ => mongoClient);
                    services.AddSingleton(_ => mongoDatabase);

                    Console.WriteLine($"HOSTBUILDER: Using configuration: {nameof(ServiceCollectionExtensions.AddMassTransitASBWithMongoDbSaga)}.");

                    services.AddMassTransitASBWithMongoDbSaga();
                });
    }
}
