﻿using MassTransit;
using MassTransitSagaRequest.Contracts;
using System;
using System.Threading.Tasks;

namespace MassTransitSagaRequest.Consumers
{
    public class OrderSubmissionConsumer : IConsumer<SubmitOrder>
    {
        public async Task Consume(ConsumeContext<SubmitOrder> context)
        {
            Console.WriteLine($"CONSUMER: Consumed order: '{context.Message.Id}'.");

            var response = new OrderSubmitted(NewId.NextGuid());

            await Task.Delay(3000);

            await context.RespondAsync(response);

            Console.WriteLine($"CONSUMER: Responded order: '{context.Message.Id}'.");
        }
    }

    public class OrderSubmissionConsumerDefinition : ConsumerDefinition<OrderSubmissionConsumer>
    {
        public OrderSubmissionConsumerDefinition()
        {
            EndpointName = "DCPC0119-a5bc9bee1477727_deletethis_create-testorder";
        }
    }
}