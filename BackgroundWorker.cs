﻿using MassTransit;
using MassTransitSagaRequest.Contracts;
using Microsoft.Extensions.Hosting;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MassTransitSagaRequest
{
    internal class BackgroundWorker : BackgroundService
    {
        readonly IBus _bus;

        public BackgroundWorker(IBus bus)
        {
            _bus = bus;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await Task.Delay(TimeSpan.FromSeconds(2), stoppingToken);

            Console.WriteLine($"Worker waiting.");

            await Task.Delay(TimeSpan.FromSeconds(5), stoppingToken);

            Console.WriteLine($"Sendin Start order.");

            var message = new StartOrder(NewId.NextGuid());
            await _bus.Publish(message, stoppingToken);

            Console.WriteLine($"Sent Start order for Id: {message.Id}.");
        }
    }
}