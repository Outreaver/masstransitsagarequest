﻿using MassTransit;
using System;

namespace MassTransitSagaRequest.Sagas
{
    public class OrderState : SagaStateMachineInstance, ISagaVersion
    {
        public Guid OrderId { get; set; }

        public Guid? RequestId { get; set; }
        public int Version { get; set; }
        public Guid CorrelationId { get; set; }
        public string CurrentState { get; set; }
    }
}