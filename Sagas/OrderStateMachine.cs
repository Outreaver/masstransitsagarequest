﻿using MassTransit;
using MassTransitSagaRequest.Contracts;
using System;
using System.Threading.Tasks;

namespace MassTransitSagaRequest.Sagas
{
    public class OrderStateMachine : MassTransitStateMachine<OrderState>
    {
        public OrderStateMachine()
        {
            InstanceState(x => x.CurrentState);

            Event(
                () => StartOrder,
                eventConfigurator => eventConfigurator.CorrelateById(context => context.Message.Id));

            Request(
                () => SubmitRequest,
                state => state.RequestId,
                config =>
                {
                    config.Timeout = TimeSpan.FromSeconds(9);
                });

            Initially(When(StartOrder)
                .Then(_ => Console.WriteLine("SAGA: Initialized."))
                .Request(SubmitRequest, _ => new SubmitOrder(NewId.NextGuid()))
                .TransitionTo(SubmitRequest.Pending));

            During(SubmitRequest.Pending,
                When(SubmitRequest.Completed)
                    //.TransitionTo(FinishingOrder)
                    .ThenAsync(async ctx =>
                    {
                        Console.WriteLine($"SAGA: Got response, current State: '{ctx.Saga.CurrentState}'.");
                        Console.WriteLine("SAGA: Starting long task for Order.");

                        await Task.Delay(TimeSpan.FromSeconds(12));

                        Console.WriteLine("SAGA: Finished long running task for Order.");
                    })
                    .TransitionTo(Completed),

                When(SubmitRequest.TimeoutExpired)
                    .Then(ctx =>
                    {
                        Console.WriteLine($"SAGA: Response timed out, current State: '{ctx.Saga.CurrentState}'.");
                    })
                    .TransitionTo(Faulted));

            WhenEnter(Completed, binder => binder.Then(ctx =>
            {
                Console.WriteLine("SAGA: Finished");
            }).Finalize());

            SetCompletedWhenFinalized();
        }

        public State FinishingOrder { get; set; }

        public State Initialized { get; set; }

        public State Faulted { get; set; }

        public State Completed { get; set; }

        public Event<StartOrder> StartOrder { get; set; } = null!;

        public Request<OrderState, SubmitOrder, OrderSubmitted> SubmitRequest { get; set; } = null!;
    }

    public class OrderStateMachineDefinition : SagaDefinition<OrderState>
    {
        public OrderStateMachineDefinition()
        {
            EndpointName = "DCPC0119-a5bc9bee1477727_deletethis_testorder_creation-saga";
        }

        protected override void ConfigureSaga(IReceiveEndpointConfigurator endpointConfigurator, ISagaConfigurator<OrderState> sagaConfigurator)
        {
            endpointConfigurator.UseInMemoryOutbox();
        }
    }
}