﻿using MassTransit;
using MassTransit.AzureServiceBusTransport;
using MassTransit.Contracts;
using MassTransitSagaRequest.Consumers;
using MassTransitSagaRequest.Contracts;
using MassTransitSagaRequest.Sagas;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using System;

namespace MassTransitSagaRequest
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddMassTransitInMemoryWithInMemorySaga(this IServiceCollection services)
        {
            services.AddMassTransit(x =>
            {
                x.AddInMemoryInboxOutbox();

                x.AddConsumer<OrderSubmissionConsumer, OrderSubmissionConsumerDefinition>();
                x.AddSagaStateMachine<OrderStateMachine, OrderState, OrderStateMachineDefinition>().InMemoryRepository();

                var schedulerEndpoint = new Uri("queue:scheduler");
                x.AddMessageScheduler(schedulerEndpoint);

                x.UsingInMemory((context, cfg) =>
                {
                    cfg.UseMessageScheduler(schedulerEndpoint);
                    cfg.ConfigureEndpoints(context);
                });
            });

            return services;
        }

        public static IServiceCollection AddMassTransitInMemoryWithMongoDbSaga(this IServiceCollection services)
        {
            services.AddMassTransit(x =>
            {
                x.AddInMemoryInboxOutbox();

                x.AddConsumer<OrderSubmissionConsumer, OrderSubmissionConsumerDefinition>();
                x.AddSagaStateMachine<OrderStateMachine, OrderState, OrderStateMachineDefinition>()
                .MongoDbRepository(r =>
                {
                    r.ClientFactory(provider => provider.GetRequiredService<IMongoClient>());
                    r.DatabaseFactory(provider => provider.GetRequiredService<IMongoDatabase>());
                    r.CollectionName = "Sagas";
                });

                var schedulerEndpoint = new Uri("queue:scheduler");
                x.AddMessageScheduler(schedulerEndpoint);

                x.UsingInMemory((context, cfg) =>
                {
                    cfg.UseMessageScheduler(schedulerEndpoint);
                    cfg.ConfigureEndpoints(context);
                });
            });

            return services;
        }

        public static IServiceCollection AddMassTransitASBWithInMemorySaga(this IServiceCollection services)
        {
            services.AddMassTransit(config =>
            {
                config.AddInMemoryInboxOutbox();

                config.AddConsumer<OrderSubmissionConsumer, OrderSubmissionConsumerDefinition>();
                config.AddSagaStateMachine<OrderStateMachine, OrderState, OrderStateMachineDefinition>().InMemoryRepository();

                config.AddServiceBusMessageScheduler();
                config.UsingAzureServiceBus((context, cfg) =>
                {
                    cfg.ConfigureASB(context);
                });
            });

            return services;
        }

        public static IServiceCollection AddMassTransitASBWithMongoDbSaga(this IServiceCollection services)
        {
            services.AddMassTransit(config =>
            {
                config.AddInMemoryInboxOutbox();

                config.AddConsumer<OrderSubmissionConsumer, OrderSubmissionConsumerDefinition>();
                config.AddSagaStateMachine<OrderStateMachine, OrderState, OrderStateMachineDefinition>()
                .MongoDbRepository(r =>
                {
                    r.ClientFactory(provider => provider.GetRequiredService<IMongoClient>());
                    r.DatabaseFactory(provider => provider.GetRequiredService<IMongoDatabase>());
                    r.CollectionName = "Sagas";
                });

                config.AddServiceBusMessageScheduler();
                config.UsingAzureServiceBus((context, cfg) =>
                {
                    cfg.ConfigureASB(context);
                });
            });

            return services;
        }

        private static void ConfigureASB(this IServiceBusBusFactoryConfigurator cfg, IBusRegistrationContext context)
        {
            cfg.UseServiceBusMessageScheduler();

            // Keep default expiry
            // In integration tests we use a short value here to cleanup stuff after running tests
            Defaults.AutoDeleteOnIdle = TimeSpan.FromMinutes(5);

            cfg.UseMessageRetry(options => options.Exponential(
                5,
                TimeSpan.FromSeconds(0),
                TimeSpan.FromSeconds(32),
                TimeSpan.FromSeconds(2)));

            cfg.ConfigureASBMessages();
            cfg.Host("secret");
            cfg.ConfigureEndpoints(context);
        }

        private static void ConfigureASBMessages(this IServiceBusBusFactoryConfigurator cfg)
        {
            cfg.Message<StartOrder>(msgTopology => msgTopology.SetEntityName("DCPC0119-a5bc9bee1477727_deletethis_testorder_creation-accepted"));

            cfg.Message<SubmitOrder>(msgTopology => msgTopology.SetEntityName("DCPC0119-a5bc9bee1477727_deletethis_create-testorder-proxy"));
            cfg.Message<Fault<SubmitOrder>>(msgTopology => msgTopology.SetEntityName("DCPC0119-a5bc9bee1477727_deletethis_create-testorder-fault"));
            cfg.Message<RequestTimeoutExpired<SubmitOrder>>(msgTopology => msgTopology.SetEntityName("DCPC0119-a5bc9bee1477727_deletethis_create-testorder-timeout"));

            cfg.Message<OrderSubmitted>(msgTopology => msgTopology.SetEntityName("DCPC0119-a5bc9bee1477727_deletethis_testorder_creation-created-responded"));

            EndpointConvention.Map<SubmitOrder>(new Uri("queue:DCPC0119-a5bc9bee1477727_deletethis_create-testorder"));
        }
    }
}
